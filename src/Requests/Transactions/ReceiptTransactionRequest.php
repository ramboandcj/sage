<?php namespace RamboAndCJ\Sage\Requests\Transactions;

use Carbon\Carbon;
use ElmhurstProjects\Core\Requests\CoreRequest;
use ElmhurstProjects\Core\Requests\RequestInterface;

class ReceiptTransactionRequest extends CoreRequest implements RequestInterface
{
    protected $request_array = [];

    protected $required_fields = ['customer_id', 'bank_id', 'cheque_value'];

    public function __construct()
    {
        $this->request_array['customer_id'] = null;
        $this->request_array['bank_id'] = null;
        $this->request_array['cheque_value'] = null;
        $this->request_array['transaction_date'] = null;
        $this->request_array['reference'] = null;
        $this->request_array['second_reference'] = null;
    }

    public function customerID(int $sage_customer_id)
    {
        $this->request_array['customer_id'] = $sage_customer_id;

        return $this;
    }

    public function bankID(int $bank_id)
    {
        $this->request_array['bank_id'] = $bank_id;

        return $this;
    }

    public function cheque_value(float $cheque_value)
    {
        $this->request_array['cheque_value'] = $cheque_value;

        return $this;
    }

    public function transactionDate(Carbon $transaction_date)
    {
        $this->request_array['transaction_date'] = $transaction_date->toISOString();

        return $this;
    }

    public function reference(string $reference)
    {
        $this->request_array['reference'] = $reference;

        return $this;
    }

    public function secondReference(string $second_reference)
    {
        $this->request_array['second_reference'] = $second_reference;

        return $this;
    }
}