<?php

namespace RamboAndCJ\Sage\Http\Controllers;

use Carbon\Carbon;
use RamboAndCJ\Sage\API\SageAuthorisationClient;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthorisationController extends Controller
{
    protected $sage_client;

    public function __construct()
    {
        $this->sage_client = new SageAuthorisationClient();
    }

    public function getAuthorisationToken()
    {
        $redirect_url = $this->sage_client->authRedirect();

        return 'Link: <a href="'.$redirect_url.'">Click here!</a>';
    }

    public function callback(Request $request)
    {
        if (!$request->has('code')) dd($request->all());

        $code = $request->get('code');

        $response = $this->sage_client->getAccessToken($code);

        $response = json_decode($response->getBody()->getContents());

        $this->sage_client->saveAccessToken($response->access_token, Carbon::now()->addMinutes(50));

        $this->sage_client->saveRefreshToken($response->refresh_token, Carbon::now()->addSeconds((int)$response->refresh_token_expires_in - 6000));

        return response()->json($response);
    }

    public function renew()
    {
        $response = $this->sage_client->renewTokenFromRefreshToken();

        dd($response);
    }
}
