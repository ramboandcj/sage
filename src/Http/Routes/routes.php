<?php

Route::any('/sage/callback', ['as' => 'sage.callback', 'uses' => '\RamboAndCJ\Sage\Http\Controllers\AuthorisationController@callback']);

Route::group(['middleware' => ['web', 'https', 'auth']], function () {
    Route::get('/sage/authorisation', ['as' => 'sage.authorisation', 'uses' => '\RamboAndCJ\Sage\Http\Controllers\AuthorisationController@getAuthorisationToken']);
    Route::get('/sage/renew', ['as' => 'sage.renew', 'uses' => '\RamboAndCJ\Sage\Http\Controllers\AuthorisationController@renew']);
});