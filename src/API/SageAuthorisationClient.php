<?php namespace RamboAndCJ\Sage\API;

use Carbon\Carbon;

class SageAuthorisationClient
{
    private $base_url;
    private $client_id;
    private $client_secret;
    private $callback_url;
    private $auth_endpoint;
    private $token_endpoint;
    private $scope;

    protected $resource_owner_id_file;

    protected $access_token_file;

    protected $refresh_token_file;

    protected $guzzle;

    public function __construct()
    {
        $this->base_url = 'https://signon.sso.services.sage.com';

        $this->client_id = env('SAGE_CLIENT_ID');
        $this->client_secret = env('SAGE_CLIENT_SECRET');
        $this->scope = env('SAGE_SCOPE');

        $this->callback_url = env('SAGE_CALLBACK');
        $this->auth_endpoint = $this->base_url . '/SSO/OAuthService/WebStartAuthorisationAttempt';
        $this->token_endpoint = $this->base_url . '/SSO/OAuthService/WebGetAccessToken';

        $this->access_token_file = storage_path('/sage/tokens/access.json');
        $this->refresh_token_file = storage_path('/sage/tokens/refresh.json');
        $this->resource_owner_id_file = storage_path('/sage/tokens/resource_id.txt');

        $this->guzzle = new \GuzzleHttp\Client();
    }

    /**
     * Returns the redirect url with required query params for hitting the
     * authorisation endpoint
     */
    public function authRedirect()
    {
        $parameters = "?response_type=code&client_id=" . urlencode($this->client_id)
            . "&redirect_uri=" . urlencode($this->callback_url)
            . "&scope=" . urlencode($this->scope);

        return $this->auth_endpoint . $parameters;
    }

    /* POST request to exchange the authorisation code for an access_token */
    public function getAccessToken($code)
    {
        $params = [
            "code" => $code,
            "grant_type" => "authorization_code",
            "redirect_uri" => $this->callback_url
        ];

        $response = $this->getToken($params);

        return $response;
    }

    /* POST request to renew the access_token */
    public function renewAccessToken($refresh_token)
    {
        $params = array("client_id" => $this->client_id,
            "client_secret" => $this->client_secret,
            "refresh_token" => $refresh_token,
            "grant_type" => "refresh_token");

        $response = $this->getToken($params);
        return $response;
    }

    /**
     * Get the token from Sage
     * @param $params
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    private function getToken($params)
    {
        try {
            if (isset($params['refresh_token'])) {
                $body = 'grant_type=' . urlencode($params['grant_type'])
                    . '&refresh_token=' . urlencode($params['refresh_token']);
            } else {
                $body = 'grant_type=' . urlencode($params['grant_type'])
                    . '&code=' . urlencode($params['code'])
                    . '&redirect_uri=' . urlencode($params['redirect_uri']);
            }

            $response = $this->guzzle->request('POST', $this->token_endpoint, [
                'verify' => false,
                'headers' => [
                    'ContentType ' => 'application/x-www-formurlencoded;charset=UTF-8',
                    'authorization' => 'Basic ' . base64_encode($this->client_id . ':' . $this->client_secret)
                ],
                'body' => $body
            ]);

            return $response;
        } catch (\Exception $e) {
            dd('Died in getToken', $e->getMessage());
        }

    }

    /**
     * Stores the access token in the file
     * @param string $access_token
     * @param Carbon $expiry_date
     */
    public function saveAccessToken(string $access_token, Carbon $expiry_date)
    {
        $file = fopen($this->access_token_file, "w");

        $content = json_encode(['access_token' => $access_token, 'expiry_date' => $expiry_date->toDateTimeString()]);

        fwrite($file, $content);

        fclose($file);
    }

    /**
     * Stores the refresh token in the file
     * @param string $refresh_token
     * @param Carbon $expiry_date
     */
    public function saveRefreshToken(string $refresh_token, Carbon $expiry_date)
    {
        $file = fopen($this->refresh_token_file, "w");

        $content = json_encode(['refresh_token' => $refresh_token, 'expiry_date' => $expiry_date->toDateTimeString()]);

        fwrite($file, $content);

        fclose($file);
    }

    /**
     * Stores the resource owner ID token in the file
     * @param $resource_owner_id
     */
    public function saveResourceOwnerID($resource_owner_id)
    {
        $file = fopen($this->resource_owner_id_file, "w");

        fwrite($file, $resource_owner_id);

        fclose($file);
    }

    /**
     * Get the access token in the file
     * @return \stdClass
     */
    public function getStoredAccessTokenObject(): \stdClass
    {
        $file = fopen($this->access_token_file, "r");

        $response = fread($file, 10000);

        fclose($file);

        return json_decode($response);
    }

    /**
     * Get the refresh token in the file
     * @return \stdClass
     */
    public function getStoredRefreshTokenObject(): \stdClass
    {
        $file = fopen($this->refresh_token_file, "r");

        $response = fread($file, 10000);

        fclose($file);

        return json_decode($response);
    }

    /**
     * Get the resource owner ID token in the file
     * @return int
     */
    public function getStoredResourceOwnerID(): int
    {
        $file = fopen($this->resource_owner_id_file, "r");

        $response = fread($file, 10000);

        fclose($file);

        return $response;
    }

    /**
     * One function to get valid access token, or it refreshes and gets a new one if required
     * @throws \Exception
     */
    public function getValidAccessToken():string
    {
        $saved_access_token_object = $this->getStoredAccessTokenObject();

        if ($saved_access_token_object->expiry_date > Carbon::now()->toDateTimeString()) return $saved_access_token_object->access_token;

        return $this->renewTokenFromRefreshToken();
    }

    /**
     * Renew the access token from saved refresh token
     * @return mixed
     * @throws \Exception
     */
    public function renewTokenFromRefreshToken():string
    {
        $saved_refresh_token_object = $this->getStoredRefreshTokenObject();

        if ($saved_refresh_token_object->expiry_date <= Carbon::now()->toDateTimeString()) throw new \Exception('Sage refresh token has now expired.');

        $response = $this->renewAccessToken($saved_refresh_token_object->refresh_token);

        $response = json_decode($response->getBody()->getContents());

        $this->saveAccessToken($response->access_token, Carbon::now()->addSeconds(3000));

        $this->saveRefreshToken($response->refresh_token, Carbon::now()->addSeconds($response->refresh_token_expires_in));

        return $response->access_token;
    }
}
