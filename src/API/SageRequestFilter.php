<?php namespace RamboAndCJ\Sage\API;
use Carbon\Carbon;

/**
 * This generates the filter query
 * Class SageRequestFilter
 * @package RamboAndCJ\Sage\API
 */
class SageRequestFilter
{
    protected $filter_string = '$filter=';

    /**
     * Get the built filter string
     * @return string
     */
    public function getFilter(): string
    {
        return $this->filter_string;
    }

    /**
     * This clears the filter, use this when iterating through and the object is being used more than once
     */
    public function clearFilter()
    {
        $this->filter_string = '$filter=';

        return $this;
    }

    /**
     * Fields contains test string
     * @param string $field
     * @param mixed $value
     * @return SageRequestFilter
     */
    public function contains(string $field, $value): SageRequestFilter
    {
        $this->ampersand();

        $this->filter_string .= "contains(" . $field . ", " . $this->convertToSageValue($value) . ")";

        return $this;
    }

    /**
     * Fields starts with test string
     * @param string $field
     * @param mixed $value
     * @return SageRequestFilter
     */
    public function startsWith(string $field, $value): SageRequestFilter
    {
        $this->ampersand();

        $this->filter_string .= "startswith(" . $field . ", " . $this->convertToSageValue($value) . ")";

        return $this;
    }

    /**
     * Fields ends with test string
     * @param string $field
     * @param mixed $value
     * @return SageRequestFilter
     */
    public function endsWith(string $field, $value): SageRequestFilter
    {
        $this->ampersand();

        $this->filter_string .= "endswith(" . $field . ", " . $this->convertToSageValue($value) . ")";

        return $this;
    }

    /**
     * Field where equals
     * @param string $field
     * @param string $operator
     * @param mixed $value
     * @return SageRequestFilter
     */
    public function where(string $field, string $operator, $value): SageRequestFilter
    {
        $this->ampersand();

        $this->filter_string .= $field . " " . $this->convertToSageOperator($operator) . " " . $this->convertToSageValue($value);

        return $this;
    }

    /**
     * Field where equals
     * @param string $field
     * @param string $operator
     * @param mixed $value
     * @return SageRequestFilter
     */
    public function andWhere(string $field, string $operator, $value): SageRequestFilter
    {
        $this->filter_string .= " and " . $field . " " . $this->convertToSageOperator($operator) . " " . $this->convertToSageValue($value);

        return $this;
    }

    /**
     * Change the value ready for sage request
     * @param mixed $value
     * @return mixed
     */
    protected function convertToSageValue($value)
    {
        if (is_string($value)) return "'" . $value . "'";

        if (is_int($value)) return $value;

        if(is_a($value, Carbon::class)) return $value->toISOString();

        return $value;
    }

    /**
     * Converts sensible operator to Sage operator
     * @param string $operator
     * @throws \Exception
     * @return string
     */
    protected function convertToSageOperator(string $operator): string
    {
        switch ($operator) {
            case '=':
                return 'eq';
                break;
            case '!=':
                return 'ne';
                break;
            case '>':
                return 'gt';
                break;
            case '<':
                return 'lt';
                break;
            case '>=':
                return 'ge';
                break;
            case '<=':
                return 'le';
                break;
            default:
                throw new \Exception('Unknown operator, could not convert to Sage');
                break;
        }
    }

    /**
     * Adds an ampersand to the query string if required
     */
    protected function ampersand()
    {
        if ($this->filter_string != '$filter=') $this->filter_string .= '&';
    }
}
