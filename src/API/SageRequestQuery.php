<?php namespace RamboAndCJ\Sage\API;

/**
 * Used to build the query for an API call
 * Class SageRequestQuery
 * @package RamboAndCJ\Sage\API
 */
class SageRequestQuery
{
    protected $select_text = '';

    protected $order_text = '';

    protected $skip_text = '';

    protected $limit_text = '';

    protected $count_text = '';

    protected $metadata_text = '';

    protected $expand_text = '';

    protected $query_string = '';

    /**
     * Get the built query string
     * @return string
     */
    public function getQuery(): string
    {
        $this->query_string = '';

        $this->addToQuery($this->select_text);
        $this->addToQuery($this->order_text);
        $this->addToQuery($this->limit_text);
        $this->addToQuery($this->skip_text);
        $this->addToQuery($this->count_text);
        $this->addToQuery($this->metadata_text);
        $this->addToQuery($this->expand_text);

        return $this->query_string;
    }

    /**
     * Adds an ampersand to the query string if required
     */
    protected function addToQuery(string $text)
    {
        if ($this->query_string != '' && $text != '') {
            $this->query_string .= '&' . $text;
        } else {
            $this->query_string .= $text;
        }
    }

    /**
     * Add select to API call
     * @param array $fields
     * @return SageRequestQuery
     */
    public function select(array $fields): SageRequestQuery
    {
        $this->select_text .= '$select=';

        foreach ($fields as $field) {
            $this->select_text .= $field . ',';
        }

        substr($this->select_text, 0, -1);

        return $this;
    }

    /**
     * Add order to the API call
     * @param string $field
     * @param string $direction
     * @return SageRequestQuery
     */
    public function orderby(string $field, string $direction): SageRequestQuery
    {
        $this->order_text = '$orderby=' . $field . ' ' . $direction;

        return $this;
    }

    /**
     * Add limit to API call
     * @param int $qty
     * @return SageRequestQuery
     */
    public function limit(int $qty): SageRequestQuery
    {
        $this->limit_text = '$top=' . $qty;

        return $this;
    }


    /**
     * Add skip qty to API call
     * @param int $qty
     * @return SageRequestQuery
     */
    public function skip(int $qty): SageRequestQuery
    {
        $this->skip_text = '$skip=' . $qty;

        return $this;
    }

    /**
     * Get the count for an API call
     * @return SageRequestQuery
     */
    public function count(): SageRequestQuery
    {
        $this->count_text = '$count=true';

        return $this;
    }

    /**
     * Get the metadata for an API call
     * @return SageRequestQuery
     */
    public function metadata(): SageRequestQuery
    {
        $this->metadata_text = '$metadata=true';

        return $this;
    }

    /**
     * expand relationship
     * @param $relationship
     * @return SageRequestQuery
     */
    public function expand(string $relationship): SageRequestQuery
    {
        $this->expand_text = '$expand=' . $relationship;

        return $this;
    }

    /**
     * Checks to see if query has an order
     * @return bool
     */
    public function hasOrderby(): bool
    {
        return $this->order_text != '';
    }
}
