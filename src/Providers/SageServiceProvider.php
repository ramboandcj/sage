<?php namespace RamboAndCJ\Sage\Providers;

use Global4Communications\Sage\Console\Commands\Contacts;
use Global4Communications\Sage\Console\Commands\SaleInvoices;
use Illuminate\Support\ServiceProvider;

class SageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Http/Routes/routes.php');

        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'sage-api');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
