<?php namespace RamboAndCJ\Sage\Responses;

abstract class AbstractSingleResponse
{
    protected $sageObject;

    /**
     * Set the object model from sage response
     * @param \stdClass $sage_response
     * @return $this
     */
    public function setResponse(\stdClass $sage_response)
    {
        $this->sageObject->setObject($sage_response);

        return $this;
    }
}