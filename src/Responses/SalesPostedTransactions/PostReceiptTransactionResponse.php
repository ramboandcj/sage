<?php namespace RamboAndCJ\Sage\Responses\SalesPostedTransactions;

use ElmhurstProjects\Core\Responses\CoreResponse;
use ElmhurstProjects\Core\Responses\ResponseInterface;

class PostReceiptTransactionResponse extends CoreResponse implements ResponseInterface
{
    protected $urn;

    public function setResponse(\stdClass $sage_response)
    {
       $this->urn = $sage_response->urn;

       return $this;
    }

    public function getURN()
    {
        return $this->urn;
    }
}