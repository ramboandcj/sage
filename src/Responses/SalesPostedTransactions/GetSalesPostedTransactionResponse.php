<?php namespace RamboAndCJ\Sage\Responses\SalesPostedTransactions;

use RamboAndCJ\Sage\Objects\SageSalesPostedTransactionObject;
use RamboAndCJ\Sage\Responses\AbstractSingleResponse;
use RamboAndCJ\Sage\Responses\ResponseSingleInterface;

class GetSalesPostedTransactionResponse extends AbstractSingleResponse implements ResponseSingleInterface
{
     public function __construct()
    {
        $this->sageObject = new SageSalesPostedTransactionObject();
    }

    public function getResponse():SageSalesPostedTransactionObject
    {
        return $this->sageObject;
    }
}