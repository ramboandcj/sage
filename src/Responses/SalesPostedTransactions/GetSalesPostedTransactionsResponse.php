<?php namespace RamboAndCJ\Sage\Responses\SalesPostedTransactions;

use RamboAndCJ\Sage\Objects\SageSalesPostedTransactionObject;
use RamboAndCJ\Sage\Responses\ResponseArrayInterface;
use RamboAndCJ\Sage\Responses\AbstractArrayResponse;

class GetSalesPostedTransactionsResponse extends AbstractArrayResponse implements ResponseArrayInterface
{
    protected function appendObject($sage_response_item)
    {
        $sage_object = new SageSalesPostedTransactionObject();

        $sage_object->setObject($sage_response_item);

        $this->sage_objects[] = $sage_object;
    }
}