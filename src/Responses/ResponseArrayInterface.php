<?php namespace RamboAndCJ\Sage\Responses;

interface ResponseArrayInterface
{
    public function setResponse(array $sage_response);

    public function getResponse();
}