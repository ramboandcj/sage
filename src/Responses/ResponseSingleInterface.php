<?php namespace RamboAndCJ\Sage\Responses;

interface ResponseSingleInterface
{
    public function setResponse(\stdClass $sage_response);

    public function getResponse();
}