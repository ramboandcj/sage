<?php namespace RamboAndCJ\Sage\Responses\Customers;

use RamboAndCJ\Sage\Objects\SageCustomerObject;
use RamboAndCJ\Sage\Responses\ResponseArrayInterface;
use RamboAndCJ\Sage\Responses\AbstractArrayResponse;

class GetCustomersResponse extends AbstractArrayResponse implements ResponseArrayInterface
{
    protected function appendObject($sage_response_item)
    {
        $sage_object = new SageCustomerObject();

        $sage_object->setObject($sage_response_item);

        $this->sage_objects[] = $sage_object;
    }
}