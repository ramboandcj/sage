<?php namespace RamboAndCJ\Sage\Responses\Customers;

use RamboAndCJ\Sage\Objects\SageCustomerObject;
use RamboAndCJ\Sage\Responses\AbstractSingleResponse;
use RamboAndCJ\Sage\Responses\ResponseSingleInterface;

class GetCustomerResponse extends AbstractSingleResponse implements ResponseSingleInterface
{
     public function __construct()
    {
        $this->sageObject = new SageCustomerObject();
    }

    public function getResponse():SageCustomerObject
    {
        return $this->sageObject;
    }
}