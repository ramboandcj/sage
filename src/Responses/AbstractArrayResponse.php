<?php namespace RamboAndCJ\Sage\Responses;

abstract class AbstractArrayResponse
{
    protected $sageObject;

    protected $sage_objects = [];

    /**
     * Set the array of object models from the sage response
     * @param array $sage_response_items
     * @return $this
     */
    public function setResponse(array $sage_response_items)
    {
        $this->sage_objects = [];

        foreach ($sage_response_items as $sage_response_item) {
            $this->appendObject($sage_response_item);
        }

        return $this;
    }

    /**
     * Get the array of the set object models
     * @return array
     */
    public function getResponse(): array
    {
        return $this->sage_objects;
    }
}