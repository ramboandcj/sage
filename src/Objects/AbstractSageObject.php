<?php namespace RamboAndCJ\Sage\Objects;

use Carbon\Carbon;

abstract class AbstractSageObject
{
    protected $raw_response;

    protected $response_array;

    /**
     * Returns the response as an array
     * @return array
     */
    public function getResponseAsArray(): array
    {
        return $this->response_array;
    }

    /**
     * Returns the response as object
     * @return \stdClass
     */
    public function getResponseAsObject(): \stdClass
    {
        return (object)$this->response_array;
    }

    /**
     * Returns the raw response from Affinity
     * @return \stdClass
     */
    public function getRawResponse(): \stdClass
    {
        return $this->raw_response;
    }

    /**
     * Get integer field
     * @param $field
     * @return int|string
     */
    protected function integerField($field): int
    {
        return is_string($field) ? (int)$field : $field;
    }

    /**
     * Get integer field
     * @param $field
     * @return int|string
     */
    protected function floatField($field): float
    {
        return is_string($field) ? (float)$field : $field;
    }

    /**
     * Get string field
     * @param $field
     * @return string
     */
    protected function stringField($field): string
    {
        return is_string($field) ? $field : '';
    }

    /**
     * Get bool field
     * @param $field
     * @return string
     */
    protected function booleanField($field):bool
    {
        if (is_string($field)) return (bool)$field;

        if (is_bool($field)) return $field;

        return false;
    }

}