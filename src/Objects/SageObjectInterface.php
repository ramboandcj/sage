<?php namespace RamboAndCJ\Sage\Objects;

use Carbon\Carbon;

interface SageObjectInterface
{
    public function setObject(\stdClass $sage_response);

    public function getResponseAsArray(): array;

    public function getResponseAsObject(): \stdClass;

    public function getRawResponse(): \stdClass;
}