<?php namespace RamboAndCJ\Sage\Objects;

use Carbon\Carbon;
use RamboAndCJ\Sage\Traits\DatesTrait;

class SageCustomerObject extends AbstractSageObject implements SageObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['id'] = null;
        $this->response_array['reference'] = null;
        $this->response_array['name'] = null;
        $this->response_array['short_name'] = null;
        $this->response_array['balance'] = null;
        $this->response_array['credit_limit'] = null;
        $this->response_array['on_hold'] = null;
        $this->response_array['active'] = null;
        $this->response_array['average_time_to_pay'] = null;
        $this->response_array['date_time_updated'] = null;
    }

    public function setObject(\stdClass $sage_response)
    {
        $this->raw_response = $sage_response;

        $this->response_array['id'] = $this->integerField($sage_response->id);
        $this->response_array['reference'] = $this->stringField($sage_response->reference);
        $this->response_array['name'] = $this->stringField($sage_response->name);
        $this->response_array['short_name'] = $this->stringField($sage_response->short_name);
        $this->response_array['balance'] = $this->floatField($sage_response->balance);
        $this->response_array['credit_limit'] = $this->floatField($sage_response->credit_limit);
        $this->response_array['on_hold'] = $this->booleanField($sage_response->on_hold);
        $this->response_array['active'] = $this->booleanField($sage_response->account_status_type) == 'AccountStatusActive';
        $this->response_array['average_time_to_pay'] = $this->integerField($sage_response->average_time_to_pay);
        $this->response_array['date_time_updated'] = $this->carbonDateFromSageField($sage_response->date_time_updated);
    }

    public function getID():? int
    {
        return $this->response_array['id'];
    }

    public function getReference():? string
    {
        return $this->response_array['reference'];
    }

    public function getName():? string
    {
        return $this->response_array['name'];
    }

    public function getShortName():? string
    {
        return $this->response_array['short_name'];
    }

    public function getBalance():? float
    {
        return $this->response_array['balance'];
    }

    public function getCreditLimit():? float
    {
        return $this->response_array['credit_limit'];
    }

    public function getOnHold():? bool
    {
        return $this->response_array['on_hold'];
    }

    public function getActive():? bool
    {
        return $this->response_array['active'];
    }

    public function getAverageTimeToPay():? int
    {
        return $this->response_array['average_time_to_pay'];
    }

    public function getDateTimeUpdated():? Carbon
    {
        return $this->response_array['date_time_updated'];
    }
}