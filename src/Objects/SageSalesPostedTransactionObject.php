<?php namespace RamboAndCJ\Sage\Objects;

use Carbon\Carbon;
use RamboAndCJ\Sage\Traits\DatesTrait;

class SageSalesPostedTransactionObject extends AbstractSageObject implements SageObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['id'] = null;
        $this->response_array['customer_id'] = null;
        $this->response_array['trader_transaction_type'] = null;
        $this->response_array['reference'] = null;
        $this->response_array['second_reference'] = null;
        $this->response_array['transaction_date'] = null;
        $this->response_array['posted_date'] = null;
        $this->response_array['due_date'] = null;
        $this->response_array['gross_value'] = null;
        $this->response_array['net_value'] = null;
        $this->response_array['tax_value'] = null;
        $this->response_array['allocated_value'] = null;
        $this->response_array['outstanding_value'] = null;
        $this->response_array['date_time_updated'] = null;
    }

    public function setObject(\stdClass $sage_response)
    {
        $this->raw_response = $sage_response;

        $this->response_array['id'] = $this->integerField($sage_response->id);
        $this->response_array['customer_id'] = $this->integerField($sage_response->customer_id);
        $this->response_array['trader_transaction_type'] = $this->stringField($sage_response->trader_transaction_type);
        $this->response_array['reference'] = $this->stringField($sage_response->reference);
        $this->response_array['second_reference'] = $this->stringField($sage_response->second_reference);
        $this->response_array['transaction_date'] = $this->carbonDateFromSageField($sage_response->transaction_date);
        $this->response_array['posted_date'] = $this->carbonDateFromSageField($sage_response->posted_date);
        $this->response_array['due_date'] = $this->carbonDateFromSageField($sage_response->due_date);
        $this->response_array['gross_value'] = $this->floatField($sage_response->document_gross_value);
        $this->response_array['net_value'] = $this->floatField($sage_response->document_goods_value);
        $this->response_array['tax_value'] = $this->floatField($sage_response->document_tax_value);
        $this->response_array['allocated_value'] = $this->floatField($sage_response->document_allocated_value);
        $this->response_array['outstanding_value'] = $this->floatField($sage_response->document_outstanding_value);
        $this->response_array['date_time_updated'] = $this->carbonDateFromSageField($sage_response->date_time_updated);
    }

    public function getID():? int
    {
        return $this->response_array['id'];
    }

    public function getCustomerID():? int
    {
        return $this->response_array['customer_id'];
    }

    public function getTraderTransactionType():? string
    {
        return $this->response_array['trader_transaction_type'];
    }

    public function isInvoiceItem():? bool
    {
        return $this->response_array['trader_transaction_type'] == 'TradingAccountEntryTypeInvoice';
    }

    public function isReceipt():? bool
    {
        return $this->response_array['trader_transaction_type'] == 'TradingAccountEntryTypePurchasePaymentSalesReceipt';
    }

    public function getReference():? string
    {
        return $this->response_array['reference'];
    }

    public function getSecondReference():? string
    {
        return $this->response_array['second_reference'];
    }

    public function getTransactionDate():? Carbon
    {
        return $this->response_array['transaction_date'];
    }

    public function getPostedDate():? Carbon
    {
        return $this->response_array['posted_date'];
    }

    public function getDueDate():? Carbon
    {
        return $this->response_array['due_date'];
    }

    public function getDateTimeUpdated():? Carbon
    {
        return $this->response_array['date_time_updated'];
    }

    public function getGrossValue():? float
    {
        return $this->response_array['gross_value'];
    }

    public function getNetValue():? float
    {
        return $this->response_array['net_value'];
    }

    public function getTaxValue():? float
    {
        return $this->response_array['tax_value'];
    }

    public function getAllocatedValue():? float
    {
        return $this->response_array['allocated_value'];
    }

    public function getOutStandingValue():? float
    {
        return $this->response_array['outstanding_value'];
    }
}